# recipe-app-api-proxy

NGINX Proxy app for recipe api. 

# usage

# Environment Variables
* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app (default: `app`)
* `APP_PORT` - Port for the app (default: `9000`)
